#include "mem.h"
#include "mem_internals.h"
#include <assert.h>

#define TEST_SIZE 128
struct block_header *get_block_header(void *ptr) {
    return (struct block_header *) (((uint8_t *)ptr) - offsetof(struct block_header, contents));
}

static void test_allocation( void ) {
    struct block_header *a = heap_init(REGION_MIN_SIZE);
    assert(a);
    struct block_header *b = get_block_header(_malloc(TEST_SIZE));
    assert(b);
    //assert(b->is_free != true);
    //assert(b->capacity.bytes == TEST_SIZE);

    heap_term();
}


static void test_free_one_block( void ) {
    struct block_header *a = heap_init(REGION_MIN_SIZE);
    assert(a);

    struct block_header *b1 = get_block_header(_malloc(TEST_SIZE));
    struct block_header *b2 = get_block_header(_malloc(TEST_SIZE));
    struct block_header *b3 = get_block_header(_malloc(TEST_SIZE));
    _free(b2);
    assert(!b1->is_free);
    assert(b2->is_free == true);
    assert(!b3->is_free);

    heap_term();
}

static void test_free_two_blocks( void ) {
    struct block_header *a = heap_init(REGION_MIN_SIZE);
    assert(a);

    struct block_header *b1 = get_block_header(_malloc(TEST_SIZE));
    struct block_header *b2 = get_block_header(_malloc(TEST_SIZE));
    struct block_header *b3 = get_block_header(_malloc(TEST_SIZE));

    _free(b3);
    assert(!b1->is_free);
    assert(!b2->is_free);
    assert(b3->is_free == true);

    _free(b2);
    assert(!b1->is_free);
    assert(b2->is_free == true);
    assert(b2->capacity.bytes == offsetof(struct block_header, contents) + TEST_SIZE * 2);

    heap_term();
}

static void test_expansion() {
    struct block_header *a = heap_init(REGION_MIN_SIZE);
    assert(a);

    struct block_header *b = get_block_header(_malloc(REGION_MIN_SIZE));
    assert(b->capacity.bytes == REGION_MIN_SIZE);
    assert(size_from_capacity(b->capacity).bytes > REGION_MIN_SIZE);

    heap_term();
}

int main( void ) {
    test_allocation();
    test_free_one_block();
    test_free_two_blocks();
    test_expansion();
    return 0;
}
